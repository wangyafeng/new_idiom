export default {
  /**
   * 默认参数
   */
  setDefaults () {
    return {
      url: '',
      data: {},
      header: {
        // 'content-type': 'application/json'
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      dataType: 'json',
      responseType: 'text',
    }
  },
  _request (mode, opt) {
    const { url, data, header, method, dataType } = Object.assign({}, this.setDefaults(), mode, opt);
    const result = new Promise((resolve, reject) => {
      wx.request({
        url: url,
        data: data,
        header: header,
        method: mode,
        dataType: dataType,
        success(data){
          if (data && data.statusCode == 200 && data.data) {
            resolve(data.data);
          } else {
            reject(data);
          }
        },
        fail (err) {
          reject(err);
        }
      });
    });
    return result;
  },
  get (opt) {
    return this._request('GET', opt);
  },
  post (opt) {
    return this._request('POST', opt);
  }
}