	// 随机数
	export function	GetRandomNum(Min, Max) {
			var Range = Max - Min;
			var Rand = Math.random();
			return Min + Math.round(Rand * Range);
	}
	// 时间格式化
	export function formatSeconds(value) {
		var secondTime = parseInt(value);// 秒
		var minuteTime = 0;// 分
		var hourTime = 0;// 小时
		if(secondTime > 60) {//如果秒数大于60，将秒数转换成整数
				//获取分钟，除以60取整数，得到整数分钟
				minuteTime = parseInt(secondTime / 60);
				//获取秒数，秒数取佘，得到整数秒数
				secondTime = parseInt(secondTime % 60);
				//如果分钟大于60，将分钟转换成小时
				if(minuteTime > 60) {
						//获取小时，获取分钟除以60，得到整数小时
						hourTime = parseInt(minuteTime / 60);
						//获取小时后取佘的分，获取分钟除以60取佘的分
						minuteTime = parseInt(minuteTime % 60);
				}
		}
		var result = "" + parseInt(secondTime) + "秒";
	
		if(minuteTime > 0) {
			result = "" + parseInt(minuteTime) + "分" + result;
		}
		if(hourTime > 0) {
			result = "" + parseInt(hourTime) + "时" + result;
		}
		return result;
	}
	//点击保存到相册
	export function save_pictures(url) {
				wx.downloadFile({
					url: url,     //仅为示例，并非真实的资源
					success(res) {
						wx.saveImageToPhotosAlbum({
							filePath:res.tempFilePath,
							success: function(data) {
							wx.showToast({
								title: "保存成功",
								icon: "success",
								duration: 2000
							});
							},
							fail: function(err) {
								if (err.errMsg === "saveImageToPhotosAlbum:fail auth deny") {
									wx.openSetting({
										success(settingdata) {
											if (settingdata.authSetting["scope.writePhotosAlbum"]) {
											wx.showToast({
												title: "获取权限成功,再次点击图片保存到相册",
												icon: "success",
												duration: 2000
											});
											} else {
											wx.showToast({
												title: "获取权限失败,再次点击获取权限",
												icon: "success",
												duration: 2000
											});
											}
										}
									});
								}
							}
						});
					}
				})
			}
	export function load_ing(){
		wx.showToast({
		  title: '加载中',
		  icon:'loading',
		  duration:1000,
		  mask:true,
		});
	}
	export function success(){
		wx.showToast({
			title: "成功",
			icon: "success",
			duration: 1000,
			mask:true,
		});
	}
	export function gettoken(){
		return wx.getStorageSync('token')
	}
		export function log_ing(appid) {
	    return new Promise ((resolve, reject) => {
				wx.getSetting({
					success(res) {
						if(res.authSetting['scope.userInfo'] == true){
							wx.login({
								success: res => {
									wx.getUserInfo({
										success: arr => {
											if(wx.getStorageSync('token') != ''){
												var tokens = wx.getStorageSync('token')
											}else{
												var tokens = ""
											}
											wx.request({
												url: appid.url,
												data: {
													appid: appid.appid,
													code: res.code,
													nick_name:arr.userInfo.nickName,
													avatar_url:arr.userInfo.avatarUrl,
													gender:arr.userInfo.gender,
													country:arr.userInfo.country,
													province:arr.userInfo.province,
													city:arr.userInfo.city,
													referrer_code:appid.referrer_code,
													token:tokens,
													source:appid.source,
												},
												method:'POST',
												header: {
													'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
												},
												success: (res)=> {
														if (res.data.code == 200) {
																resolve(res);
																wx.setStorageSync('token', res.data.data.token);
																wx.setStorageSync('is_new', res.data.data.is_new);													
																wx.setStorageSync('invite_code', res.data.data.invite_code);													
															} else {//返回错误提示信息
																reject(res.data);
															}
												}
											})
										},
									})
								}
							})
						}else{
							wx.login({
								success: res => {
									// console.log(res)
											wx.request({
												url: appid.url,
												data: {
													appid: appid.appid,
													code: res.code,
													nick_name:'',
													avatar_url:'',
													gender:'',
													country:'',
													province:'',
													city:'',
													referrer_code:appid.referrer_code,
													token:'',
													source:appid.source,
												},
												method:'POST',
												header: {
													'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
												},
												success: (res)=> {
														if (res.data.code == 200) {
																resolve(res);
																wx.setStorageSync('token', res.data.data.token);
																wx.setStorageSync('is_new', res.data.data.is_new);													
																wx.setStorageSync('invite_code', res.data.data.invite_code);													
															} else {//返回错误提示信息
																reject(res.data);
															}
												}
											})
								}
							})
						}
					}
				})
	    })
		}
		

  export default {
	  GetRandomNum,   // 产生一个随机数 传 最大与最小
		save_pictures,  // 保存到相册  传一个接口
		load_ing,       // 加载中提示框 不可击穿
		success,        // 成功提示框 不可击穿
		gettoken,       // 获取 缓存的token
		log_ing,         // 首页进入时调用 // 授权时调用 
		formatSeconds,    // 时间格式化
  }
// 	sdk.log_in({
// 		url:api.login,
// 		appid:api.appid
// 	}).then(()=>{
// 		wx.navigateTo({
// 			url: "../details/main?id="+data+'&rt='+rt
// 		})
// 	})

// 分享
// 	onShareAppMessage(){
// 	    return {
// 	            title: '纠结终结者',
// 	            imageUrl:'https://st.fcadx.cn/4cad4480a562bbc0961ffcde2a022a14.png',
// 	            path: '/pages/index/main',
// 	    }
// 	},