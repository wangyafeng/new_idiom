/**
 * 地址配置文件
 */
const appid = 'wx89f148c9d04d557c'; // 新版成语
const versionnumber = '1.2.1';
// const host = 'http://dev01.fcadx-inc.cn:8902/'; // 域名
const host = 'https://mp.fcadx.cn/'; // 域名
// const login = 'http://dev01.fcadx-inc.cn:8906/basic/user/login'; // 登录
// const create = 'http://dev01.fcadx-inc.cn:8906/basic/user/formid/create'; // 收集formid
const api = {
    appid,
    versionnumber,
    // login, // 登陆
    // create, //收集formid
    login:`${host}basic/user/login`, // 登陆
    create:`${host}basic/user/formid/create`, //收集formid
    index:`${host}basic/ad/search`, // 任务列表
    completed:`${host}basic/ad/completed`, // 任务提交
    profile:`${host}answer/user/profile`, // 用户详情
    invite:`${host}answer/invite/index`, // 用户邀请列表
    receive:`${host}answer/card/receive`, // 用户领取答题卡
    view:`${host}answer/question/view`, // 问题详情 答题页
    verify:`${host}answer/question/verify`, // 问题回答 提交
    config:`${host}basic/media/config`, // 配置
    double:`${host}answer/reward/double`, // 问题回答正确翻倍
    newuserdouble:`${host}answer/newuser/double`, // 新用户奖励翻倍
    jiance:`https://engine.fcadx.cn/e/mp`, // 监测
};
export default api